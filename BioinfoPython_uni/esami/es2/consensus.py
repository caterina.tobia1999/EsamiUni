def load_profile(filename):
    """Legge un file di profili e restituisce una lista di coppie (residuo,profilo)."""
    f = open(filename)
    profile = []
    for row in f:
        data = row.split()
        profile.append((data[1], data[2]))
    f.close()
    return profile

def composition(seq):
    """Calcola la composizione aminoacidica di una sequenza."""
    diz = {}
    for c in seq:
        #if not c in diz:
        #    diz[c] = 0
        #diz[c] += 1
        if c in diz:
            diz[c] += 1
        else:
            diz[c] = 1
    return diz

def most_frequent(diz):
    """Prende in ingresso un dizionario di coppie aminoacido,frequenza e restituisce l'aminoacido piu' frequente."""
    amino = ""
    freq = 0
    for (k,v) in diz.items():
        if v > freq:
            freq = v
            amino = k
    return amino

def ref2consensus(ref, cons):
    """Prende in ingresso un elemento della sequenza di riferimento 
ed il corrispondente elemento della sequenza consenso 
e restituisce un carattere che specifica il tipo di modifica avvenuta."""
    if ref == cons:
        return "C"
    if cons == '-':
        return "D"
    return "M"
    # if ref == cons:
    #     return "C"
    # elif cons == '-':
    #     return "D"
    # else:
    #     return "M"

def compute_consensus_sequence(profile):
    """Prende in ingresso una lista di coppie (residuo, profilo) 
e stampa la sequenza di riferimento affiancata alla sequenza consenso 
e alla sequenza delle modifiche del consenso rispetto al riferimento."""
    # stampo intestazione
    print("ref\tcons\tmod\n")
    for (char, seq) in profile:
        # calcolo il carattere consenso
        cons = most_frequent(composition(seq))
        change = ref2consensus(char, cons)
        print(char + "\t" + cons + "\t" + change)


filename = input("Inserire nome file: ")
profile = load_profile(filename)
compute_consensus_sequence(profile)
