def load_data(filename):
    """Legge un file e restituisce un dizonario nome campo -> elenco di valori di quel campo."""
    f = open(filename)
    diz = {}

    # estraggo le chiavi
    header = f.readline().split()

    # inizializzo il dizionario
    for k in header:
        diz[k] = []

    #print(diz)

    # processo i dati
    for row in f:
        data = row.split()
        #print(data)
        for i in range(len(data)):
            #print(i)
            field = header[i]
            #print(header[i])
            diz[field].append(data[i]) 
            #print(diz[field])
        
    f.close()
    return diz

def compute_mean(values):
    """Calcola la media di una lista di stringhe di valori."""

    mean = 0    
    for val in values:
        mean += float(val)
    return mean / len(values)

def compute_frequencies(values):
    """Calcola le frequenze di una lista di valori, 
    restituisce un dizionario valore -> numero di occorrenze."""

    diz = {}
    for val in values:
        if not val in diz:
            diz[val] = 1
        else:
            diz[val] += 1 
    return diz

def arrange_by_class(class_values, attribute_values):
    """Prende in ingresso l'elenco dei valori associati ad una classificazione 
e l'elenco dei valori associati ad un attributo.
Restituisce un dizionario con i valori di classificazione come chiavi e l'elenco dei valori di attributo con tale classificazione come valore."""
    
    diz = {}

    for (clas, attr) in zip(class_values, attribute_values):
    
        if not clas in diz:
            diz[clas] = [attr]
        else:
            diz[clas].append(attr)

    return diz

def compute_stats(data, classification, attribute, is_continuous):
    """Prende in ingresso il dizionario dei dati, un campo da usare 
come classificazione ed uno da usare come attributo, e l'indicazione
se l'attributo e' continuo o meno. Stampa le statistiche relative 
all'attributo divise per valore di classificazione."""

    # estraggo i dati aggregati per valore di classificazione
    data4class = arrange_by_class(data[classification], data[attribute])

    # stampo intestazione
    print(classification + "\t" + attribute)

    if is_continuous:
        for k,v in data4class.items():
            print("%s\t%.1f" %(k,compute_mean(v)))
    else:
        for k,v in data4class.items():
            freqs = compute_frequencies(v)
            freq_str = " ".join(["%s->%d" %(attr,val) for attr,val in freqs.items()])
            print(k + "\t" + freq_str)

filename = input("Inserire nome file: ")
classification = input("Inserire campo classificazione: ")
attribute  = input("Inserire campo attributo: ")

continuous_str = input("Attributo continuo? (S/N): ")

while continuous_str != "S" and continuous_str != "N":
    continuous_str = input("Rispondere solo S o N: ")

if continuous_str  == 'S':
    is_continuous = True
else:
    is_continuous = False

data = load_data(filename)
compute_stats(data, classification, attribute, is_continuous)
