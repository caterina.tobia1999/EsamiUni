def load_ss(filename):
    """Prende in ingresso un nome di file. 
Restituisce una lista di coppie <elmento ss, lunghezza>."""

    f = open(filename)

    ss = []

    for row in f:    
        #if ("STRAND","HELIX","TURN") in row:
        if "STRAND" in row or "HELIX" in row or "TURN" in row:
            data = row.split()
            ss_elem = data[1]
            ss_len = int(data[3]) - int(data[2]) + 1
            ss.append((ss_elem, ss_len))
    
    f.close()
    return ss

def update_dic(dic, key, count):

    if not key in dic:
        dic[key] = [count, 1]
    else:
        dic[key][0] += count
        dic[key][1] += 1

def compute_means(dic):

    mean_dic = {}
    for el in dic.keys():
        mean_dic[el] = dic[el][0] / dic[el][1]
    return mean_dic


def compute_mean_ss_len(ss_list):
    """Prende in ingresso una lista di coppie <elmento ss, lunghezza> e restituisce un dizionario 
    elemento di ss -> lunghezza media."""

    ss_means = {}

    # accumulo somma di lunghezze e numero di occorrenze
    for (el,el_len) in ss_list:
        update_dic(ss_means, el, el_len)

    # calcolo medie
    return compute_means(ss_means)


def compute_mean_num_consecutive(ss_list):
    """Prende in ingresso una lista di coppie <elmento ss, lunghezza> e restituisce un dizionario 
elemento di ss -> numero medio di occorrenze consecutive."""

    ss_consec = {}

    ss_el, ss_occ = ss_list[0][0], 1
    
    for el in ss_list[1:]:
        new_ss_el = el[0]
        if new_ss_el == ss_el:
            # aggiorno il numero di occorrenze di ss_el
            ss_occ += 1
        else:
            # inserisco ss_el nel dizionario
            update_dic(ss_consec, ss_el, ss_occ)
            # passo a new_ss_el
            ss_el, ss_occ = new_ss_el, 1

    # aggiorno con ultimo elemento
    update_dic(ss_consec, ss_el, ss_occ)

    # calcolo medie
    return compute_means(ss_consec)


filename = input("Inserire nome file: ")

ss_list = load_ss(filename)
print(compute_mean_ss_len(ss_list))
print(compute_mean_num_consecutive(ss_list))
