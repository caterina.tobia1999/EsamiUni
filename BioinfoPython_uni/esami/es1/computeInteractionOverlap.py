def loadInteractions(filename):
    """Prende in ingresso un file di interazioni. 
Restituisce un dizionario con elemento regolatore come chiave 
e insieme di elementi da esso regolati come valore.""" 

    f = open(filename)
    diz = {}
    
    # salto intestazione
    f.readline()

    for row in f:
        data = row.split()
        # aggiungo elemento regolatore se non presente
        if not data[0] in diz:
            diz[data[0]] = set()
        # aggiungo elemento regolato al valore
        diz[data[0]].add(data[1])

    f.close()
    return diz

def filterInteractions(interaction_dict, thres):
    """Prende in ingresso un dizionario elemento regolatore -> targets ed un valore di soglia.
Restituisce un dizionario con gli elementi regolatori che hanno almeno thres target."""
    filtered_dict = {}
    for (k,v) in interaction_dict.items():
        if len(v) >= thres:
            filtered_dict[k] = v 
    return filtered_dict

def printCommonInteractions(interaction_dict):
    """Prende in ingresso un dizionario elemento regolatore -> targets ed un valore di soglia. 
 Stampa il numero di interazioni in comune tra ogni coppia di elementi regolatori."""
    for k1 in interaction_dict.keys():
        for k2 in interaction_dict.keys():
            # estraggo gli elementi regolati in comune tra k1 e k2
            shared_int = interaction_dict[k1].intersection(interaction_dict[k2])
            print(k1,k2,len(shared_int))

def printCommonInteractionTable(int_dic):
    """Prende in ingresso un dizionario elemento regolatore -> targets ed un valore di soglia. 
 Stampa una tabella con il numero di interazioni in comune tra ogni coppia di elementi regolatori."""

    regulators = int_dic.keys()

    # stampa intestazione tabella
    print("reg\t" + "\t".join(regulators))

    for k1 in regulators:
        print(k1 + "\t" + "\t".join([str(len(int_dic[k1].intersection(int_dic[k2]))) for k2 in regulators]))

def printCommonInteractionTableReadable(int_dic):
    """Prende in ingresso un dizionario elemento regolatore -> targets ed un valore di soglia. 
 Stampa una tabella con il numero di interazioni in comune tra ogni coppia di elementi regolatori."""

    regulators = int_dic.keys()

    # stampa intestazione tabella
    print("reg\t" + "\t".join(regulators))

    for k1 in regulators:
        row = k1
        for k2 in regulators:
            row += "\t" + str(len(int_dic[k1].intersection(int_dic[k2])))
        print(row)


filename = input("Inserire nome file: ")
thres = int(input("Inserire valore soglia: "))

int_dict = loadInteractions(filename)
filtered_dict = filterInteractions(int_dict, thres)
printCommonInteractionTableReadable(filtered_dict)
